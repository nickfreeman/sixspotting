# SixSpotting via Tor
[SixSpotting](https://game.flyingpenguintech.org) is a website which gamifies the [IPv6](https://en.wikipedia.org/wiki/IPv6) adoption.
This repo contains a script which iterates through all exit-nodes of the [Tor Project](https://www.torproject.org/) and for each tries to connect to the check-in page of SixSpotting.

# Get started
- Create an account or login on [SixSpotting](https://game.flyingpenguintech.org/login)
- Get the cookie `session_id` from the browser console (F12)
- Make sure Tor is installed
- Run `./torbulkexitlist.sh [session_id]` with your `session_id`
- You can run `tail -f tmp/log.txt` to read the log of Tor

# What exactly is happening here?
The configuration file of Tor is called `torrc`. First we set the `ControlSocket` and `CookieAuthFile` to a local folder, so we can start tor without any privileges. Next we start Tor in the background, get the [pid](https://en.wikipedia.org/wiki/Process_identifier) and wait for 3 seconds.

To get a list of all Tor exit-nodes, we execute `LINES=$(wget -qO- https://check.torproject.org/torbulkexitlist)`. This gives us the list of all Tor nodes which looks something like that:
```
93.95.230.253
23.137.249.143
185.244.195.103
185.220.101.18
185.220.101.76
...
```

Now we can iterate over these. For each `LINE in $LINES` we set the allowed exit-nodes to be just the current `LINE` by restoring torrc, adding `ExitNodes $LINE` and then reloading Tor.
Now we can make a http-request to `https://game.flyingpenguintech.org/checkin` via Tor using [curl](https://curl.se/).
To do that we set the proxy to be `socks5://localhost:9050`. To force Tor to use a new connection every time, we use a new password by adding `-U new:$(openssl rand -hex 10)`.

Sadly IPv6 is still somehow broken somewhere (at least it just didn't work for me), so we need to tell curl the address to connect to by adding `--connect-to game.flyingpenguintech.org:443:\[$ipv6\]:443`.
The variable `$ipv6` has been set to be the IPv6-address of `game.flyingpenguintech.org` at the beginning of the script using `ipv6=$(dig game.flyingpenguintech.org AAAA +short || echo "2600:3c03:e001:600::1")`.

Last but not least we set the session_id cookie by adding `--cookie "session_id=$sessionid"`.

You can take a look at my blogpost here: [nickfreeman.de/2022/11/08/sixspotting-via-tor.html](https://www.nickfreeman.de/2022/11/08/sixspotting-via-tor.html)
