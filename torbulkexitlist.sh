#!/bin/bash
if [ $# -ne 1 ]; then
        echo "./torbulkexitlist.sh [session_id]"
	exit 1
fi
sessionid=$1

ipv6=$(dig game.flyingpenguintech.org AAAA +short || echo "2600:3c03:e001:600::1")

mkdir tmp
chmod 700 tmp
echo "ControlSocket $PWD/tmp/control" > tmp/torrc_pre
echo "ControlSocketsGroupWritable 1" >> tmp/torrc_pre
echo "CookieAuthentication 1" >> tmp/torrc_pre
echo "CookieAuthFile $PWD/tmp/control.authcookie" >> tmp/torrc_pre
echo "CookieAuthFileGroupReadable 1" >> tmp/torrc_pre
echo "StrictNodes 1" >> tmp/torrc_pre
cp tmp/torrc_pre tmp/torrc

tor -f tmp/torrc > tmp/log.txt &
pid=$!
sleep 3

LINES=$(wget -qO- https://check.torproject.org/torbulkexitlist)
for LINE in $LINES
do
  cp tmp/torrc_pre tmp/torrc
  echo "ExitNodes $LINE" >> tmp/torrc
  kill -n HUP "$pid"
  curl -sk --connect-timeout 5 --proxy socks5://localhost:9050 -U new:$(openssl rand -hex 10) --connect-to game.flyingpenguintech.org:443:\[$ipv6\]:443 'https://game.flyingpenguintech.org/checkin' --cookie "session_id=$sessionid" | grep "<p>"
done

kill "$pid"
rm -r tmp
